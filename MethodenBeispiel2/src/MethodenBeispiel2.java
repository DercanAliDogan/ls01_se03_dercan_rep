
public class MethodenBeispiel2 {

	public static void main(String[] args) {
	
		int erg = add(3,4);
		
		int erg2 = summe(3,4,5);
		
	   System.out.println("ergebnis : " + erg);
       System.out.println("ergebnis : " + erg2);
	}

	public static int add(int zahl1, int zahl2) {
	    int erg = zahl1 + zahl2;
	    
	    //System.out.println(zahl1 + " + " + zahl2 + " = "+ erg);
	    
	    return erg;
    }	
 

   public static int summe( int zahl1, int zahl2, int zahl3 ) {
	  
	   int erg = zahl1 + zahl2 + zahl3;
	   
	   //System.out.println(zahl1 + " + " + zahl2 + " + " + zahl3 + " = "+ erg);
	   
	   return erg;
   }
   }