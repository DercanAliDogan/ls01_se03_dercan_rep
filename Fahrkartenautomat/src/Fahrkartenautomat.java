﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);

       double zuZahlenderBetrag = fahrkartenbestellungErfassen("Zu zahlender Betrag (EURO): ");
       double anzahlFahrkarten = fahrkartenbestellungErfassen("Anzahl der Fahrkarten: ");
       double eingezahlterGesamtbetrag = fahrkartenBezahlen("Noch zu zahlen: %.2f Euro%nEingabe (mind. 5Ct, höchstens 2 Euro): ", zuZahlenderBetrag, anzahlFahrkarten);
        
    		   
       	   
        fahrkartenAusgeben();
        rueckgeldAusgeben(zuZahlenderBetrag, anzahlFahrkarten, eingezahlterGesamtbetrag);
       
        
       
    } 
       public static double fahrkartenbestellungErfassen(String text) {
    	   Scanner tastatur = new Scanner(System.in);
    	   
    	   System.out.print(text);
    	   
    	   double erfasst = tastatur.nextDouble();
    	   return erfasst;
    	   
       }
 
       
       public static double fahrkartenBezahlen(String text, double zuZahlenderBetrag, double anzahlFahrkarten) {
    	   Scanner tastatur = new Scanner(System.in);
    	   
    	   double zuZahlen = 0.0, eingezahlterGesamtbetrag = 0.0;

           while (eingezahlterGesamtbetrag < (zuZahlenderBetrag*anzahlFahrkarten)) {
        	   System.out.printf(text, (zuZahlenderBetrag*anzahlFahrkarten) - eingezahlterGesamtbetrag);
        	   double eingeworfeneMünze = tastatur.nextDouble();
        	  
           eingezahlterGesamtbetrag += eingeworfeneMünze;   
    	   zuZahlen = (zuZahlenderBetrag*anzahlFahrkarten) - eingezahlterGesamtbetrag;
           }
          
           
           
    	   return eingezahlterGesamtbetrag;
       }   
       

       
       
       
       //Fahrkartenkosten
       //
       
       //System.out.print("Zu zahlender Betrag (EURO): ");
       //zuZahlenderBetrag = tastatur.nextDouble();
       
       // Fahrkartenanzahl
       // -----------
       //System.out.print("Anzahl der Fahrkarten: ");
       //anzahlFahrkarten = tastatur.nextDouble();
       

       // Geldeinwurf
       // -----------
       /*eingezahlterGesamtbetrag = 0.0;
       while(eingezahlterGesamtbetrag < zuZahlenderBetrag*anzahlFahrkarten)
       {
    	   System.out.printf("Noch zu zahlen: %.2f Euro%n", (zuZahlenderBetrag*anzahlFahrkarten - eingezahlterGesamtbetrag));
    	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
    	   eingeworfeneMünze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneMünze;
       }
*/
       // Fahrscheinausgabe
       // -----------------
       public static void fahrkartenAusgeben() 
   {	   
    	   
    	   
       System.out.println("\nFahrschein wird ausgegeben");
       for (int i = 0; i < 8; i++)
       {
          System.out.print("=");
          try {
			Thread.sleep(250);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       }
       System.out.println("\n\n");
   }
       // Rückgeldberechnung und -Ausgabe
       // -------------------------------
       
       
       public static void rueckgeldAusgeben(double zuZahlenderBetrag, double anzahlFahrkarten, double eingezahlterGesamtbetrag) 
   {   
       
       double rückgabebetrag = eingezahlterGesamtbetrag - (zuZahlenderBetrag*anzahlFahrkarten);
       if(rückgabebetrag > 0.00)
       {
    	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f Euro%n" , rückgabebetrag);
    	   System.out.println("wird in folgenden Münzen ausgezahlt:");

           while(rückgabebetrag >= 2.00) // 2 EURO-Münzen
           {
        	  System.out.println("2 EURO ");
	          rückgabebetrag -= 2.00;
	          
           }
           while(rückgabebetrag >= 1.00) // 1 EURO-Münzen
           {
        	  System.out.println("1 EURO ");
	          rückgabebetrag -= 1.00;
           }
           while(rückgabebetrag >= 0.50) // 50 CENT-Münzen
           {
        	  System.out.println("50 CENT ");
	          rückgabebetrag -= 0.50;
           }
           while(rückgabebetrag >= 0.20) // 20 CENT-Münzen
           {
        	  System.out.println("20 CENT ");
 	          rückgabebetrag -= 0.20;
           }
           while(rückgabebetrag >= 0.10) // 10 CENT-Münzen
           {
        	  System.out.println("10 CENT ");
	          rückgabebetrag -= 0.10;
           }
           while(rückgabebetrag >= 0.04)// 5 CENT-Münzen
           {
        	  System.out.println("5 CENT ");
 	          rückgabebetrag -= 0.04;
           }

       }

       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir wünschen Ihnen eine gute Fahrt.");
    }
}